# orangepi server

## install

  1. copy ``.env.example`` to ``.env`` and modify vars
  2. install docker
  3. run ``docker-compose up --build -d``

## access

- portainer: <http://localhost:9000>
- jellyfin: <http://localhost:8096>
- homeassistant: <http://localhost:8123>
- owncloud: <http://localhost:8080>
